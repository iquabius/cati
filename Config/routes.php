<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
    // Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
    Router::connect('/', array('controller' => 'cati', 'action' => 'home'));
    Router::connect('/cati-2013', array('controller' => 'cati', 'action' => 'cati2013'));
    Router::connect('/contato', array('controller' => 'cati', 'action' => 'contact'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

    Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
    Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
    Router::connect('/cadastrar', array('controller' => 'users', 'action' => 'add'));
    Router::connect('/resetar-senha', array('controller' => 'users', 'action' => 'forgot_pwd'));
    Router::connect('/minha-conta', array('controller' => 'users', 'action' => 'my_account'));
    Router::connect('/minha-conta/editar', array('controller' => 'users', 'action' => 'edit_my_data'));
    Router::connect('/minha-conta/excluir', array('controller' => 'users', 'action' => 'delete_my_account'));
    Router::connect('/minha-conta/comprovar-pagamento', array('controller' => 'users', 'action' => 'verify_payment'));
    // Router::connect('/cadastrar', array('controller' => 'users', 'action' => 'add'));

    Router::connect('/palestras', array('controller' => 'lectures', 'action' => 'index'));
    Router::connect('/palestras/adicionar', array('controller' => 'lectures', 'action' => 'add'));
    Router::connect('/palestras/editar/*', array('controller' => 'lectures', 'action' => 'edit'), array('id' => '[0-9]+'));
    Router::connect('/palestras/detalhes/*', array('controller' => 'lectures', 'action' => 'view'), array('id' => '[0-9]+'));
    Router::connect('/palestras/excluir/*', array('controller' => 'lectures', 'action' => 'delete'), array('id' => '[0-9]+'));

    Router::connect('/minicursos', array('controller' => 'courses', 'action' => 'index'));
    Router::connect('/minicursos/adicionar', array('controller' => 'courses', 'action' => 'add'));
    Router::connect('/minicursos/editar/*', array('controller' => 'courses', 'action' => 'edit'), array('id' => '[0-9]+'));
    Router::connect('/minicursos/detalhes/*', array('controller' => 'courses', 'action' => 'view'), array('id' => '[0-9]+'));
    Router::connect('/minicursos/excluir/*', array('controller' => 'courses', 'action' => 'delete'), array('id' => '[0-9]+'));

    Router::connect('/usuarios', array('controller' => 'users', 'action' => 'index'));
    Router::connect('/usuarios/editar/*', array('controller' => 'users', 'action' => 'edit'), array('id' => '[0-9]+'));
    Router::connect('/usuarios/detalhes/*', array('controller' => 'users', 'action' => 'view'), array('id' => '[0-9]+'));
    Router::connect('/usuarios/excluir/*', array('controller' => 'users', 'action' => 'delete'), array('id' => '[0-9]+'));

    Router::connect('/dias', array('controller' => 'days', 'action' => 'index'));
    Router::connect('/dias/adicionar', array('controller' => 'days', 'action' => 'add'));
    Router::connect('/dias/editar/*', array('controller' => 'days', 'action' => 'edit'), array('id' => '[0-9]+'));
    Router::connect('/dias/detalhes/*', array('controller' => 'days', 'action' => 'view'), array('id' => '[0-9]+'));
    Router::connect('/dias/excluir/*', array('controller' => 'days', 'action' => 'delete'), array('id' => '[0-9]+'));
    Router::connect('/dias/verificar-presenca/*', array('controller' => 'days', 'action' => 'verify_attendance'), array('id' => '[0-9]+'));
    Router::connect('/dias/remover-presenca-verificada/*', array('controller' => 'days', 'action' => 'remove_attendance_verification'), array('id' => '[0-9]+'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
