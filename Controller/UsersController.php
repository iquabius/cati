<?php

class UsersController extends AppController {
    public $components = array('Email' ,'Session');
    public $helpers    = array('Html','Form','Session');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add', 'forgot_pwd', 'reset_pwd'); // Letting users register themselves
    }

    public function my_account() {
        $this->set('title_for_layout', 'Minha Conta');
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if (!$user) {
            $this->Session->setFlash('Primeiro você precisa fazer o login!', 'flash/error');
            return $this->redirect(array('action' => 'login'));
        }
        $user = $this->User->read(null, $user['id']);
        $user = $user['User'];
        $this->set('user', $user);
    }

    public function edit_my_data() {
        $this->set('title_for_layout', 'Editar Meus Dados');
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if (!$user) {
            $this->Session->setFlash('Primeiro você precisa fazer o login!', 'flash/error');
            return $this->redirect(array('action' => 'login'));
        }

        $this->User->id = $user['id'];
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Seus dados foram salvos', 'flash/success');
                return $this->redirect(array('action' => 'my_account'));
            }
            $this->Session->setFlash('Seus dados não foram salvos, tente novamente', 'flash/error');
        } else {
            $this->request->data = $this->User->read(array('name', 'lastname', 'cpf', 'institution', 'payment_status'), $user['id']);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete_my_account() {
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if (!$user) {
            $this->Session->setFlash('Primeiro você precisa fazer o login!', 'flash/error');
            return $this->redirect(array('action' => 'login'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->id = $user['id'];
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->User->delete()) {
                $this->Session->setFlash('Sua conta foi excluída!', 'flash/success');
                $this->logout();
            }
        } else {
            return $this->redirect(array('action' => 'login'));
        }
    }

    public function verify_payment() {
        $this->set('title_for_layout', 'Verificar Pagamento');
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if (!$user) {
            $this->Session->setFlash('Primeiro você precisa fazer o login!', 'flash/error');
            return $this->redirect(array('action' => 'login'));
        }

        $this->User->id = $user['id'];
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $filename = $this->request->data['User']['payment_confirm_image']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = date('d-m-Y') . '_' . md5(date('c')) . '.' . $ext;
            move_uploaded_file(
                $this->request->data['User']['payment_confirm_image']['tmp_name'],
                IMAGES . 'payment_confirmations' . DS . basename($filename)
            );
            $this->request->data['User']['payment_confirm_image'] = $filename;
            $this->request->data['User']['payment_confirm_date'] = date('Y-m-d H:i:s');
            $this->request->data['User']['payment_status'] = 'pending';

            if ($this->User->validates(array('fieldList' => array('payment_confirm_image')))) {
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash('Nossa equipe irá conferir seu pagamento em no máximo 24 horas!', 'flash/success');
                    return $this->redirect(array('action' => 'my_account'));
                }
            }
            $this->Session->setFlash('Ocorreu um erro, tente novamente!', 'flash/error');
            $this->request->data = $this->User->read(array('payment_status', 'payment_confirm_image', 'payment_confirm_date'), $user['id']);
            unset($this->request->data['User']['password']);
            $this->set('user', $this->request->data['User']);
        } else {
            $this->request->data = $this->User->read(array('payment_status', 'payment_confirm_image', 'payment_confirm_date'), $user['id']);
            unset($this->request->data['User']['password']);
            $this->set('user', $this->request->data['User']);
        }
    }

    public function index() {
        $this->set('title_for_layout', 'Usuários');
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if ((!$user || !($user['role'] == 'admin' || $user['role'] == 'support'))) {
            $this->render('/Cati/not-permited');
            return;
        }

        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        $this->set('title_for_layout', 'Detalhes do Usuário');
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->read(null, $id);

        // Check if the user has permission to access this action
        $loggedUser = $this->Session->read('Auth.User');
        if ((!$loggedUser || !($loggedUser['role'] == 'admin' || $loggedUser['role'] == 'support'))) {
            // if the user is a speaker it can be shown
            // if ($user['User']['role'] != 'speaker') {
                $this->render('/Cati/not-permited');
                return;
            // }
        }

        $this->set('user', $user);
    }

    public function add() {
        $this->set('title_for_layout', 'Inscrição');
        // don't let the user register if he/she is logged
        if ($user = $this->Session->read('Auth.User')) {
            $this->Session->setFlash(sprintf('Você já está logado como "%s"!', $user['email']), 'flash/warning');
            return $this->redirect(array('controller' => 'cati', 'action' => 'home'));
        }

        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                // $this->Session->setFlash('Cadastrado com sucesso!','flash/success', array('Themed'=>'Cakestrap'));
                $this->Session->setFlash('Cadastrado com sucesso!','flash/success');
                return $this->redirect(array('controller' => 'cati', 'action' => 'home'));
            }
            $this->Session->setFlash(__('Erro ao cadastrar. Tente novamente!'), 'flash/error');
        }
    }

    public function edit($id = null) {
        $this->set('title_for_layout', 'Editar Dados do Usuário');
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        if ((!$user || !($user['role'] == 'admin' || $user['role'] == 'support'))/* && $user['id'] != $id*/) {
            $this->render('/Cati/not-permited');
            return;
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Os dados do usuário foram salvos', 'flash/success');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('Os dados do usuário não foram salvos, tente novamente', 'flash/error');
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        // Check if the user has permission to access this action
        $user = $this->Session->read('Auth.User');
        $hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
        if (!$hasPermission) {
            $this->render('/Cati/not-permited');
            return;
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($hasPermission && $this->User->delete()) {
            $this->Session->setFlash('Usuário excluído!', 'flash/success');
            return $this->redirect(array('action' => 'index'));
        }
        if (!$hasPermission) {
            $this->Session->setFlash(__('Você não tem permissão para executar essa ação!'), 'flash/error');
        } else {
            $this->Session->setFlash(__('Ocorreu um erro, tente novamente!'), 'flash/error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function login() {
        $this->set('title_for_layout', 'Login');
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect(array('action' => 'my_account'));
                // return $this->redirect($this->Auth->redirect());
            }
            $this->Session->setFlash(__('Email ou senha inválido, tente novamente!'), 'flash/error');
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    function forgot_pwd(){
        $this->set('title_for_layout', 'Recuperar Senha');
        //$this->layout="signup";
        $this->User->recursive=-1;
        if (!empty($this->request->data)) {
            if (empty($this->request->data['User']['email'])) {
                $this->Session->setFlash('Nos forneça seu email', 'flash/error');
            } else {
                $email = $this->data['User']['email'];
                $fu = $this->User->find('first', array('conditions'=>array('User.email' => $email)));
                if ($fu) {
                    //debug($fu);
                    // if ($fu['User']['active']) {
                        // $key = Security::hash(String::uuid(),'sha512',true);
                        $key = Security::hash(String::uuid(),'sha1',true);
                        $hash = sha1($fu['User']['email'].rand(0,100));
                        $url = Router::url( array('controller'=>'users','action'=>'reset_pwd'), true ).'/'.$key.'#'.$hash;
                        $ms = $url;
                        $ms = wordwrap($ms,1000);
                        //debug($url);
                        $fu['User']['token_hash'] = $key;
                        $this->User->id = $fu['User']['id'];
                        if($this->User->saveField('token_hash', $fu['User']['token_hash'])) {

                            //============Email================//
                            /* SMTP Options */
                            $this->Email->smtpOptions = array(
                                'timeout'=>'30',
                                'host' => 'ssl://smtp.gmail.com',
                                'port' => 465,
                                'username' => 'cati@unemat.br',
                                'password' => 'angelina2013',
                            );
                            $this->Email->template = 'resetpw';
                            $this->Email->from     = 'CATI <accounts@example.com>';
                            $this->Email->to       = $fu['User']['name'] . '<' . $fu['User']['email'] . '>';
                            $this->Email->subject  = 'Crie uma nova senha para sua conta do CATI';
                            $this->Email->sendAs   = 'both';

                            $this->Email->delivery = 'smtp';
                            $this->set('ms', $ms);
                            $this->Email->send();
                            $this->set('smtp_errors', $this->Email->smtpError);
                            $this->Session->setFlash('Verifique seu email para criar uma nova senha', 'flash/success');

                            //============EndEmail=============//
                        // } else {
                            // $this->Session->setFlash('Ocorreu um interno, tente novamente mais tarde!', 'flash/error');
                        // }
                    } else {
                        $this->Session->setFlash('Essa conta ainda não está ativada!', 'flash/error');
                    }
                } else {
                    $this->Session->setFlash('O email que você forneceu não está cadastrado em nosso banco de dados', 'flash/error');
                }
            }
        }
    }

    function reset_pwd($token = null){
        $this->set('title_for_layout', 'Criar Nova Senha');
        //$this->layout="Login";
        $this->User->recursive = -1;
        if (!empty($token)) {
            $u = $this->User->findBytoken_hash($token);
            if ($u) {
                $this->User->id = $u['User']['id'];
                if (!empty($this->data)) {
                    $this->User->data = $this->data;
                    $this->User->data['User']['email'] = $u['User']['email'];
                    $new_hash = sha1($u['User']['email'].rand(0,100));//created token
                    $this->User->data['User']['token_hash'] = $new_hash;
                    if ($this->User->validates(array('fieldList' => array('password', 'password_confirm')))) {
                        if ($this->User->save($this->User->data)) {
                            $this->Session->setFlash('Sua nova senha foi cadastrada', 'flash/success');
                            $this->redirect(array('controller'=>'users', 'action'=>'login'));
                        }
                    } else {
                        $this->set('errors', $this->User->invalidFields());
                    }
                }
            } else {
                $this->Session->setFlash('Link quebrado ou expirado. Tente novamente. O link para cadastrar nova senha só pode ser usado uma vez.', 'flash/error');
            }
        } else {
            $this->redirect('/');
        }
    }

    /*public function test_email() {
        $this->send_mail('wryck7@gmail.com', 'Josias', '123456');
        echo 'sent??'; die;
    }*/

    /*public function send_mail($receiver = null, $name = null, $pass = null) {
        $confirmation_link = "http://" . $_SERVER['HTTP_HOST'] . $this->webroot . "users/login/";
        $message = 'Hi,' . $name . ', Your Password is: ' . $pass;
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('gmail');
        $email->from('yourUsername@gmail.com');
        $email->to($receiver);
        $email->subject('Mail Confirmation');
        $email->send($message . " " . $confirmation_link);
    }*/
}
