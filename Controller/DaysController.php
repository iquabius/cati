<?php
App::uses('AppController', 'Controller');
/**
 * Days Controller
 *
 * @property Day $Day
 * @property PaginatorComponent $Paginator
 */
class DaysController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
        // Check if the user has permission to access this controller
        $user = $this->Session->read('Auth.User');
        if ((!$user || !($user['role'] == 'admin' || $user['role'] == 'support'))) {
            $this->render('/Cati/not-permited');
            return;
        }
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->set('title_for_layout', 'Lista de Dias');
		$this->Day->recursive = 0;
		$this->set('days', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->set('title_for_layout', 'Detalhes do Dia');
		if (!$this->Day->exists($id)) {
			throw new NotFoundException(__('Invalid day'));
		}
		$options = array('conditions' => array('Day.' . $this->Day->primaryKey => $id));
		$this->set('day', $this->Day->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        $this->set('title_for_layout', 'Adicionar Dia');
		if ($this->request->is('post')) {
			$this->Day->create();
			if ($this->Day->save($this->request->data)) {
				$this->Session->setFlash(__('O dia foi salvo :)'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O dia não pode ser salvo :(. Tente novamente.'), 'flash/error');
			}
		}
		$users = $this->Day->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->set('title_for_layout', 'Editar Dia');
        $this->Day->id = $id;
		if (!$this->Day->exists($id)) {
			throw new NotFoundException(__('Invalid day'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Day->save($this->request->data)) {
				$this->Session->setFlash(__('O dia foi salvo :)'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O dia não pode ser salvo :(. Tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Day.' . $this->Day->primaryKey => $id));
			$this->request->data = $this->Day->find('first', $options);
		}
		$users = $this->Day->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Day->id = $id;
		if (!$this->Day->exists()) {
			throw new NotFoundException(__('Invalid day'));
		}
		if ($this->Day->delete()) {
			$this->Session->setFlash(__('O dia foi excluído'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('O dia não foi excluído'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function verify_attendance($id) {
        $this->set('title_for_layout', 'Verificar presença');
        $this->Day->id = $id;
		if (!$this->Day->exists($id)) {
			throw new NotFoundException(__('Invalid day'));
		}

		$options = array('conditions' => array('Day.' . $this->Day->primaryKey => $id));
		$day = $this->Day->find('first', $options);
		$this->set('day', $day['Day']);

		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['User']['check_user'])) {
				$this->set('check_user', 1);
				if (!empty($this->request->data['User']['user_id']) && is_numeric($this->request->data['User']['user_id'])) {
					$options = array(
						'conditions' => array('User.id' => $this->request->data['User']['user_id']),
						'fields' => array('email', 'payment_status', 'name', 'lastname', 'cpf'),
					);
					$user = $this->Day->User->find('first', $options);
					if (!$user) {
						$this->Session->setFlash('O ID fornecido não existe', 'flash/error');
					}

					$day_id  = $day['Day']['id'];
					$user_id = $user['User']['id'];
					// check if the attendance is already verified
					$check = $this->Day->query(sprintf(
						'SELECT * FROM `users_days` WHERE `user_id` = %s AND `day_id` = %s',
						$user_id,
						$day_id
					));
					if ($check) {
						$this->set('already_verified', true);
					}

					$this->set('confirm_attendance', 1);
					$this->set('user', $user);
				} else {
					$this->Session->setFlash('Houve um erro, confira o ID do usuário e tente novamente', 'flash/error');
				}
			} else {
				$this->set('confirm_attendance', 1);

				// check if the user really exists
				$options = array(
					'conditions' => array('User.id' => $this->request->data['User']['user_id']),
					'fields' => array('id'),
				);
				$user = $this->Day->User->find('first', $options);
				if (!$user) {
					$this->Session->setFlash('Houve um erro, tente novamente', 'flash/error');
					$this->redirect(array('action' => 'verify_attendance', $id));
				}

				$day_id  = $day['Day']['id'];
				$user_id = $user['User']['id'];

				// check if the attendance is already verified
				$check = $this->Day->query(sprintf('SELECT * FROM `users_days` WHERE `user_id` = %s AND `day_id` = %s', $user_id, $day_id));
				if ($check) {
					$this->Session->setFlash('A presença desse usuário já está confirmada', 'flash/warning');
					$this->redirect(array('action' => 'verify_attendance', $id));
				}

				try {
					$this->Day->query(sprintf('INSERT INTO `users_days` (`user_id`, `day_id`) VALUES (%s, %s)', $user_id, $day_id));
					$this->Session->setFlash(__('A presença do usuário foi confirmada'), 'flash/success');
				} catch (Exception $e) {
					$this->Session->setFlash(__('Presença não confirmada. Tente novamente.'), 'flash/error');
				}

				$this->redirect(array('action' => 'verify_attendance', $id));
			}
		} else {
			$options = array('conditions' => array('Day.' . $this->Day->primaryKey => $id));
			$this->request->data = $this->Day->find('first', $options);
		}
	}

	public function remove_attendance_verification($day_id, $user_id) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$bond = $this->Day->query(sprintf(
			'SELECT * FROM `users_days` WHERE `user_id` = %s AND `day_id` = %s',
			$user_id,
			$day_id
		));

		if ($bond) {
			$this->Day->query(sprintf('DELETE FROM `users_days` WHERE `id` = %s', $bond[0]['users_days']['id']));
			$this->Session->setFlash('A confirmação de presença foi desfeita', 'flash/success');
		} else {
			$this->Session->setFlash('A confirmação de presença NÂO foi desfeita, tente novamente', 'flash/error');
		}
		$this->redirect(array('action' => 'view', $day_id));

		debug($day_id);
		debug($user_id);
		die;
	}
}
