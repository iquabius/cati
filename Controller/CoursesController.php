<?php
App::uses('AppController', 'Controller');
/**
 * Courses Controller
 *
 * @property Course $Course
 * @property PaginatorComponent $Paginator
 */
class CoursesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->set('title_for_layout', 'Minicursos');
		$this->Course->recursive = 0;
		$this->set('courses', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->set('title_for_layout', 'Detalhes do Minicurso');
		if (!$this->Course->exists($id)) {
			throw new NotFoundException(__('Invalid course'));
		}
		$options = array('conditions' => array('Course.' . $this->Course->primaryKey => $id));
		$this->set('course', $this->Course->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        $this->set('title_for_layout', 'Cadastrar Minicurso');
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		if (!$user || !($user['role'] == 'admin' || $user['role'] == 'support')) {
			$this->render('/Cati/not-permited');
		}

		if ($this->request->is('post')) {
			$this->Course->create();
			if ($this->Course->save($this->request->data)) {
				$this->Session->setFlash(__('The course has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The course could not be saved. Please, try again.'), 'flash/error');
			}
		}
		// $users = $this->Course->User->find('list');
		$users = $this->Course->User->find('list', array('conditions' => array('User.role' => 'speaker')));
		$this->set(compact('users', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->set('title_for_layout', 'Editar Minicurso');
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		if (!$user || !($user['role'] == 'admin' || $user['role'] == 'support')) {
			$this->render('/Cati/not-permited');
			return;
		}

        $this->Course->id = $id;
		if (!$this->Course->exists($id)) {
			throw new NotFoundException(__('Invalid course'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Course->save($this->request->data)) {
				$this->Session->setFlash('As modificações foram salvas!', 'flash/success');
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash('Ocorreu um erro, tente novamente.', 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Course.' . $this->Course->primaryKey => $id));
			$this->request->data = $this->Course->find('first', $options);
		}
		// $users = $this->Course->User->find('list');
		$users = $this->Course->User->find('list', array('conditions' => array('User.role' => 'speaker')));
		$this->set(compact('users', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		$hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
		if (!$hasPermission) {
			$this->render('/Cati/not-permited');
			return;
		}

		$this->Course->id = $id;
		if (!$this->Course->exists()) {
			throw new NotFoundException(__('Invalid course'));
		}
		if ($hasPermission && $this->Course->delete()) {
			$this->Session->setFlash(__('Minicurso Excluído!'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		if (!$hasPermission) {
			$this->Session->setFlash(__('Você não tem permissão para executar essa ação!'), 'flash/error');
		} else {
			$this->Session->setFlash(__('Ocorreu um erro, tente novamente!'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}
}
