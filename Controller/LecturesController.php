<?php
App::uses('AppController', 'Controller');
/**
 * Lectures Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LecturesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->set('title_for_layout', 'Palestras');
		$this->Lecture->recursive = 0;
		$this->set('lectures', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->set('title_for_layout', 'Detalhes da Palestra');
		if (!$this->Lecture->exists($id)) {
			throw new NotFoundException(__('Invalid lecture'));
		}
		$options = array('conditions' => array('Lecture.' . $this->Lecture->primaryKey => $id));
		$this->set('lecture', $this->Lecture->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        $this->set('title_for_layout', 'Cadastrar Palestra');
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		if (!$user || !($user['role'] == 'admin' || $user['role'] == 'support')) {
			$this->render('/Cati/not-permited');
		}

		if ($this->request->is('post')) {
			$this->Lecture->create();
			if ($this->Lecture->save($this->request->data)) {
				$this->Session->setFlash('Palestra adicionada.', 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Ocorreu um erro, tente novamente.', 'flash/error');
			}
		}
		// $users = $this->Lecture->User->find('list');
		$users = $this->Lecture->User->find('list', array('conditions' => array('User.role' => 'speaker')));
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->set('title_for_layout', 'Editar Palestra');
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		if (!$user || !($user['role'] == 'admin' || $user['role'] == 'support')) {
			$this->render('/Cati/not-permited');
			return;
		}

        $this->Lecture->id = $id;
		if (!$this->Lecture->exists($id)) {
			throw new NotFoundException(__('Invalid lecture'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Lecture->save($this->request->data)) {
				$this->Session->setFlash('As modificações foram salvas!', 'flash/success');
				return $this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash('Ocorreu um erro, tente novamente.', 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Lecture.' . $this->Lecture->primaryKey => $id));
			$this->request->data = $this->Lecture->find('first', $options);
		}
		// $users = $this->Lecture->User->find('list');
		$users = $this->Lecture->User->find('list', array('conditions' => array('User.role' => 'speaker')));
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
		// Check if the user has permission to access this action
		$user = $this->Session->read('Auth.User');
		$hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
		if (!$hasPermission) {
			$this->render('/Cati/not-permited');
			return;
		}

		$this->Lecture->id = $id;
		if (!$this->Lecture->exists()) {
			throw new NotFoundException(__('Invalid lecture'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($hasPermission && $this->Lecture->delete()) {
			$this->Session->setFlash('Palestra excluída.', 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		if (!$hasPermission) {
			$this->Session->setFlash(__('Você não tem permissão para executar essa ação!'), 'flash/error');
		} else {
			$this->Session->setFlash(__('Ocorreu um erro, tente novamente!'), 'flash/error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
