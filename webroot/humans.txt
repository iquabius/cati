# humanstxt.org/

/* TEAM */

    Developer: Josias Duarte Busiquia
    Twitter: @JosiasBusiquia
    Facebook: fb.com/josias.busiquia
    From: Barra do Bugres, Mato Grosso, Brazil

/* THANKS */

    Content and Images: CATI team

/* SITE */

    Standards: PHP5, MySQL 5, HTML5, Javascript, CSS3
    Components: CakePHP, Twitter Bootstrap, jQuery, Font Awesome
    Software: Sublime Text, MySQL Workbench
    Last update: 2013/10/10
    Language: Portuguese
