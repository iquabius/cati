<?php
App::uses('AppModel', 'Model');

class User extends AppModel {
    public $displayField = 'email';

    public $hasMany = array(
        'Course' => array(
            'className' => 'Course',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Lecture' => array(
            'className' => 'Lecture',
            'foreignKey' => 'user_id',
            'dependent' => false,
            // 'conditions' => array('User.role' => 'support'),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            // 'finderQuery' => 'SELECT User.* from users as User WHERE User.role = "support";',
            'counterQuery' => ''
        )
    );


    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Day' => array(
            'className' => 'Day',
            'joinTable' => 'users_days',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'day_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você não pode deixar esse campo em branco',
            ),
            'isEmail' => array(
                'rule' => array('email'),
                'message' => 'Digite um email válido',
            ),
            'isUnique' => array (
                'rule' => 'isUnique',
                'message' => 'Esse email já está cadastrado',
            ),
            'maxLength' => array(
                'rule'    => array('maxLength', 255),
                'message' => 'O email deve no máximo 255 caracteres',
            ),
        ),
        'email_confirm' => array(
            'confirmation' => array(
                'rule' => array('equalToField', 'email'),
                'message' => 'Seus e-mails não correspondem. Tente novamente.',
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você não pode deixar esse campo em branco',
            ),
            'maxLength' => array(
                'rule'    => array('maxLength', 20),
                'message' => 'Sua senha deve ter no máximo 20 caracteres',
            ),
            'minLength' => array(
                'rule'    => array('minLength', 6),
                'message' => 'Sua senha deve ter no mínimo 6 caracteres',
            ),
        ),
        // For reset password
        'password_confirm' => array(
            'required' => array(
                'rule' => array('equalToField', 'password'),
                'message' => 'As senhas não correspondem. Tente novamente.',
            ),
        ),
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você não pode deixar esse campo em branco',
            ),
            'maxLength' => array(
                'rule'    => array('maxLength', 127),
                'message' => 'O nome deve no máximo 127 caracteres',
            ),
        ),
        'lastname' => array(
            'maxLength' => array(
                'rule'    => array('maxLength', 127),
                'message' => 'O sobrenome deve no máximo 127 caracteres',
                'allowEmpty' => true,
            ),
        ),
        'institution' => array(
            'maxLength' => array(
                'rule'    => array('maxLength', 127),
                'message' => 'Esse campo deve ter no máximo 127 caracteres',
                'allowEmpty' => true,
            ),
        ),
        'phone' => array(
            'maxLength' => array(
                'rule'    => array('maxLength', 12),
                'message' => 'Esse campo deve ter no máximo 12 caracteres',
                'allowEmpty' => true,
            ),
        ),
        'cpf' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você não pode deixar esse campo em branco',
            ),
            'cpf' => array(
                'rule' => array('validateCPF'),
                'message' => 'O CPF que você informou é inválido',
            ),
            'isUnique' => array (
                'rule' => 'isUnique',
                'message' => 'Esse CPF já está cadastrado',
            ),
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'support', 'speaker', 'guest')),
                'message' => 'Selecione um papel válido',
                'allowEmpty' => false
            ),
        ),
        'payment_confirm_image' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você não selecionou um arquivo',
            ),
            'extension' => array(
                'rule'    => array('extension', array('pdf', 'jpeg', 'png', 'jpg')),
                'message' => 'Extenções aceitas: pdf, jpeg, png, jpg.',
            ),
            'size' => array(
                'rule' => array('fileSize', '<=', '1MB'),
                'message' => 'Tamanho máximo aceito: 1MB!',
            )
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'],'md5', true);;
            // $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
            // $this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'],'blowfish');;
        }
        return true;
    }

    /**
     * Método usado para confirmar os dois campos de email
     *
     * @param  array $array
     * @param  string $field
     * @return boolean
     */
    public function equalToField($array, $field) {
        return strcmp($this->data[$this->alias][key($array)], $this->data[$this->alias][$field]) == 0;
    }

    function validateCPF($cpf = null) {
        // Verifica se um número foi informado
        if(empty($cpf['cpf'])) {
            return false;
        }
        $cpf = $cpf['cpf'];

        // Elimina possivel mascara
        $cpf = ereg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if (
            $cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }
}
