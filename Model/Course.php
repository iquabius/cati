<?php
App::uses('AppModel', 'Model');
/**
 * Course Model
 *
 * @property User $User
 * @property User $User
 */
class Course extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 255),
				'message' => 'No máximo 255',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 1024),
				'message' => 'No máximo 1024',
			),
		),
		'price' => array(
			'money' => array(
				'rule' => array('money'),
				'message' => 'Digite um valor de moeda válido',
			),
		),
        'user_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você deve selecionar um ministrante',
            ),
        ),
		'schedule' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 512),
				'message' => 'No máximo 512',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	/*public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'courses_users',
			'foreignKey' => 'course_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);*/

}
