<?php
App::uses('AppModel', 'Model');

class Lecture extends AppModel {

    public $displayField = 'title';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Digite um título para a palestra',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'maxLength' => array(
                'rule'    => array('maxLength', 255),
                'message' => 'O título de ter no máximo 255 caracteres',
            ),
        ),
        'description' => array(
            /*'allowEmpty' => array(
                'allowEmpty' => true,
            ),*/
            'maxLength' => array(
                'rule'    => array('maxLength', 1024),
                'message' => 'A descrição deve ter no máximo 1024 caracteres',
                'allowEmpty' => true,
            ),
        ),
        'user_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Você deve selecionar um palestrante',
            ),
        ),
        'schedule' => array(
            /*'allowEmpty' => array(
                'allowEmpty' => true,
            ),*/
            'maxLength' => array(
                'rule'    => array('maxLength', 512),
                'message' => 'A programação deve ter no máximo 512 caracteres',
                'allowEmpty' => true,
            ),
        ),
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            // 'conditions' => array('User.role' => 'support'),
            'fields' => '',
            'order' => ''
        )
    );
}
