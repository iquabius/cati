<!-- form -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <li class="list-group-item"><?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $this->Form->value('Day.id')), null, __('Tem certeza que deseja excluir "%s"?', $this->Form->value('Day.title'))); ?></li>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">

    <div class="days form">
      <?php echo $this->Form->create('Day', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
      <fieldset>

        <h2>Editar Dia</h2>

        <div class="form-group">
          <?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->label('title', 'Título');?>
          <?php echo $this->Form->input('title', array('class' => 'form-control')); ?>
        </div><!-- .form-group -->

      </fieldset>
      <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>

    </div><!-- /.form -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
