<!-- form -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">

    <div class="days form">
    <?php if(!isset($confirm_attendance)): ?>
      <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
      <fieldset>

        <h2>Verificar Presença: <b><?php echo $day['title'] ?></b></h2>
        <?php echo $this->Form->hidden('check_user'); ?>
        <div class="form-group">
          <?php echo $this->Form->input('user_id', array('label' => 'ID do Usuário', 'type' => 'text', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

      </fieldset>
      <?php echo $this->Form->submit('Verificar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>

    <?php else: ?>
      <h2>Verificar Presença: <b><?php echo $day['title'] ?></b></h2>

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <h3>Usuário</h3>
          <tbody>
            <tr>
              <td><strong>ID</strong></td>
              <td>
                <?php echo h($user['User']['id']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Nome do Usuário</strong></td>
              <td>
                <?php echo h($user['User']['name'] . ' ' . $user['User']['lastname']); ?>
              </td>
            </tr>
            <tr class="<?php echo h(__d('cake', $user['User']['payment_status'] . '_css_class')); ?>" title="<?php echo h(__d('cake', $user['User']['payment_status'] . '_description')); ?>">
              <td><strong>Status do Pagamento</strong></td>
              <td>
                <?php echo h(__d('cake', $user['User']['payment_status'])); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Email</strong></td>
              <td>
                <?php echo h($user['User']['email']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>CPF</strong></td>
              <td>
                <?php echo h($user['User']['cpf']); ?>
              </td>
            </tr>
          </tbody>
        </table><!-- /.table table-striped table-bordered -->
      </div><!-- /.table-responsive -->

      <?php if (isset($already_verified)): ?>
        <div class="alert alert-warning">
          A presença desse usuário já está confirmada!
        </div>
          <?php echo $this->Html->link('Voltar', array(), array('class' => 'btn btn-large btn-primary')); ?>
      <?php else: ?>
        <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
          <?php echo $this->Form->hidden('user_id', array('value' => $user['User']['id'])); ?>
        <?php echo $this->Form->submit('Confirmar presença', array('class' => 'btn btn-large btn-success')); ?>
        <?php echo $this->Form->end(); ?>
      <?php endif; ?>

    <?php endif; ?>

    </div><!-- /.form -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
