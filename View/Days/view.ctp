<!-- view -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
<?php
    // Check if the user is an admin or support
    $loggedUser = $this->Session->read('Auth.User');
    $hasPermission = ($loggedUser && ($loggedUser['role'] == 'admin' || $loggedUser['role'] == 'support'));
    if ($hasPermission) :
?>
        <li class="list-group-item"><?php echo $this->Html->link('Editar Dia', array('action' => 'edit', $day['Day']['id']), array('class' => '')); ?> </li>
        <li class="list-group-item"><?php echo $this->Form->postLink('Excluir Dia', array('action' => 'delete', $day['Day']['id']), array('class' => ''), __('Tem certeza que deseja excluir "%s"?', $day['Day']['title'])); ?> </li>
<?php endif; ?>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .span3 -->

  <div id="page-content" class="col-sm-9">

    <div class="days view">

      <h2>Dia</h2>

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td><strong>Título</strong></td>
              <td>
                <?php echo h($day['Day']['title']); ?>
                &nbsp;
              </td>
            </tr>
          </tbody>
        </table><!-- /.table table-striped table-bordered -->
      </div><!-- /.table-responsive -->

    </div><!-- /.view -->


      <div class="related">

        <h3>Lista de presentes</h3>

        <?php if (!empty($day['User'])): ?>

          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th><?php echo __('Id'); ?></th>
                  <th><?php echo __('Email'); ?></th>
                  <th><?php echo __('Nome'); ?></th>
                  <th><?php echo __('Sobrenome'); ?></th>
                  <th><?php echo __('CPF'); ?></th>
                  <th class="actions2"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $i = 0;
                  foreach ($day['User'] as $user): ?>
                  <tr>
                      <td><?php echo $user['id']; ?></td>
                      <!-- <td><?php echo $user['email']; ?></td> -->
                      <td><?php echo $this->Html->link($user['email'], array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => '')); ?></td>
                      <td><?php echo $user['name']; ?></td>
                      <td><?php echo $user['lastname']; ?></td>
                      <td><?php echo $user['cpf']; ?></td>
                      <td class="actions2">
                        <?php echo $this->Form->postLink(__('Remover'), array('controller' => 'days', 'action' => 'remove_attendance_verification', $day['Day']['id'], $user['id']), array('class' => 'btn btn-default btn-xs btn-danger'), __('Tem certeza que deseja desfazer essa verificação de presença?')); ?>
                      </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table><!-- /.table table-striped table-bordered -->
          </div><!-- /.table-responsive -->

        <?php else: ?>
          <p class="text-muted"><i>Nenhum usuário confirmou presença neste dia.</i></p>
        <?php endif; ?>


        <div class="actions">
          <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('Adicionar usuário à lista de presentes'), array('action' => 'verify_attendance', $day['Day']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>        </div><!-- /.actions -->

      </div><!-- /.related -->

  </div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
