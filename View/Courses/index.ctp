<!-- index -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">
    <div class="alert alert-warning">
        - Poderão inscrever-se nos minicursos apenas os participantes que já tem sua inscrição confirmada no congresso. A taxa de inscrição do CATI 2013 é de R$ 40,00.
        <br><br>
        - As inscrições para os minicursos serão realizadas apenas presencialmente, durante o credenciamento do evento, no dia 22 de outubro.
    </div>

    <div class="courses index">

      <h2>Minicursos</h2>

      <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('title', 'Título'); ?></th>
              <th><?php echo $this->Paginator->sort('user_id', 'Ministrante'); ?></th>
              <th><?php echo $this->Paginator->sort('price', 'Preço (R$)'); ?></th>
<?php
  $user = $this->Session->read('Auth.User');
  $hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
?>
              <th class="<?php echo $hasPermission ? 'actions' : 'actions2' ?>"></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($courses as $course): ?>
              <tr>
                <td><?php echo h($course['Course']['title']); ?>&nbsp;</td>
<?php if ($hasPermission): ?>
                <td>
                  <?php echo $this->Html->link($course['User']['name'] . ' ' . $course['User']['lastname'], array('controller' => 'users', 'action' => 'view', $course['User']['id'])); ?>
                </td>
<?php else: ?>
                <td><?php echo h($course['User']['name'] . ' ' . $course['User']['lastname']); ?>&nbsp;</td>
<?php endif; ?>
                <td><?php echo h($course['Course']['price']); ?>&nbsp;</td>
                <td class="<?php echo $hasPermission ? 'actions' : 'actions2' ?>">
                  <?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $course['Course']['id']), array('class' => 'btn btn-default btn-xs')); ?>
<?php if ($hasPermission): ?>
                  <?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $course['Course']['id']), array('class' => 'btn btn-default btn-xs')); ?>
                  <?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $course['Course']['id']), array('class' => 'btn btn-default btn-xs btn-danger'), __('Tem certeza que deseja excluir "%s"?', $course['Course']['title'])); ?>
<?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div><!-- /.table-responsive -->

      <p><small>
        <?php
        echo $this->Paginator->counter(array(
        'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de um total de {:count}, começando do registro {:start}, terminando em {:end}')
        ));
        ?>
      </small></p>

      <ul class="pagination">
        <?php
          echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
          echo $this->Paginator->next(__('Próximo') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
      </ul><!-- /.pagination -->

    </div><!-- /.index -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
