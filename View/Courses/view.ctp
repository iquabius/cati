<!-- view -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
<?php
    // Check if the user is an admin or support
    $user = $this->Session->read('Auth.User');
    $hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
    if ($hasPermission) :
?>
        <li class="list-group-item"><?php echo $this->Html->link('Editar Minicurso', array('action' => 'edit', $course['Course']['id']), array('class' => '')); ?> </li>
        <li class="list-group-item"><?php echo $this->Form->postLink('Excluir Minicurso', array('action' => 'delete', $course['Course']['id']), array('class' => ''), __('Tem certeza que deseja excluir "%s"?', $course['Course']['title'])); ?> </li>
<?php endif; ?>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .span3 -->

  <div id="page-content" class="col-sm-9">

    <div class="courses view">

      <h2>Minicurso</h2>

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td><strong>Título</strong></td>
              <td>
                <?php echo h($course['Course']['title']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Descrição</strong></td>
              <td>
                <?php echo h($course['Course']['description']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Preço (R$)</strong></td>
              <td>
                <?php echo h($course['Course']['price']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Ministrante</strong></td>
              <td>
                <?php echo h($course['User']['name'] . ' ' . $course['User']['lastname']); ?>
              </td>
            </tr>
            <tr>
              <td><strong>Programação</strong></td>
              <td>
                <?php echo h($course['Course']['schedule']); ?>
              </td>
            </tr>
          </tbody>
        </table><!-- /.table table-striped table-bordered -->
      </div><!-- /.table-responsive -->

    </div><!-- /.view -->

  </div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
