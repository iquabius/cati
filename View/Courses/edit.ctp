<!-- form -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <li class="list-group-item"><?php echo $this->Form->postLink('Excluir', array('action' => 'delete', $this->Form->value('Course.id')), null, __('Tem certeza que deseja excluir "%s"?', $this->Form->value('Course.title'))); ?></li>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">

    <div class="courses form">
      <?php echo $this->Form->create('Course', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
      <fieldset>

        <h2>Editar Minicurso</h2>

        <div class="form-group">
          <?php echo $this->Form->input('title', array('label' => 'Título', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('description', array('label' => 'Descrição', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('price', array('type' => 'text', 'label' => 'Preço (R$)', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('user_id', array('label' => 'Ministrante', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('schedule', array('label' => 'Programação', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

      </fieldset>
      <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>

    </div><!-- /.form -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
