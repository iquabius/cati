
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <li class="list-group-item"><?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Tem certeza que deseja excluir "%s"?', $this->Form->value('User.email'))); ?></li>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">

    <div class="users form">
      <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
      <fieldset>
        <h2><?php echo __('Editar Usuário'); ?></h2>

        <div class="form-group">
          <?php echo $this->Form->input('email', array('label' => 'Email', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('name', array('label' => 'Nome', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('lastname', array('label' => 'Sobrenome', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('role', array('type' => 'select', 'label' => 'Função', 'class' => 'form-control', 'options' => array('admin' => 'Administrador', 'support' => 'Suporte', 'speaker' => 'Ministrante', 'guest' => 'Visitante'))); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('cpf', array('label' => 'CPF', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php echo $this->Form->input('institution', array('label' => 'Intituição', 'class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
          <?php
            echo $this->Form->input('payment_status', array(
              'type' => 'select',
              'label' => 'Status de Pagamento',
              'class' => 'form-control',
              'options' => array(
                'verified' => h(__d('cake', 'verified')),
                'pending' => h(__d('cake', 'pending')),
                'unverified' => h(__d('cake', 'unverified')),
              )
            ));
          ?>
        </div><!-- .form-group -->

      </fieldset>

      <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>

    </div><!-- /.form -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
