<!-- view -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
<?php
    // Check if the user is an admin or support
    $loggedUser = $this->Session->read('Auth.User');
    $hasPermission = ($loggedUser && ($loggedUser['role'] == 'admin' || $loggedUser['role'] == 'support'));
?>
        <li class="list-group-item"><?php echo $this->Form->postLink('Excluir Minha Conta', array('action' => 'delete_my_account'), array('class' => 'text-danger'), 'Tem certeza que deseja excluir sua conta? Todos os seus dados serão apagados de nosso banco de dados e não poderão ser recuperados. Seu email e CPF poderão ser usados para criar uma nova conta.'); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link('Comprovar Pagamento', array('action' => 'verify_payment')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link('Editar Meus Dados', array('action' => 'edit_my_data')); ?> </li>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .span3 -->

  <div id="page-content" class="col-sm-9">

        <div class="alert alert-warning">
          <b>Dados da conta bancária para o pagamento:</b></br>
          <b>Banco:</b> Banco do Brasil</br>
          <b>Agência:</b> 3196-8</br>
          <b>Conta Corrente:</b> 20264-9</br>
          <b>Títular da Conta:</b> Rodrigo Tasca Pavão</br>
        </div><!-- /.alert alert-warning -->

<?php if ($user['payment_status'] != 'verified'): ?>
  <?php if ($user['payment_status'] == 'pending'): ?>
      <div class="alert alert-warning">
        A confirmação de seu pagamento está em andamento, se você fazer o upload de um novo arquivo, ele substituirá o anterior!
      </div><!-- /.alert alert-warning -->
  <?php endif; ?>
      <?php echo $this->Form->create('User', array('type' => 'file', 'inputDefaults' => array('label' => false), 'role' => 'form')); ?>
        <fieldset>
          <h2>Confirmação de Pagamento</h2>

          <div class="form-group">
            <?php echo $this->Form->input('payment_confirm_image', array('type' => 'file', 'class' => 'form-control', 'label' => 'Arquivo de Imagem/Pdf')); ?>
            <span class="help-block">Extenções aceitas: pdf, jpeg, png, jpg.</span>
            <span class="help-block">Tamanho máximo aceito: 1MB.</span>
          </div><!-- .form-group -->

        </fieldset>
        <?php echo $this->Form->submit('Enviar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>
<?php else: ?>
    <div class="alert alert-success">
      Seu Pagamento já está confirmado!
    </div><!-- /.alert alert-success -->
<?php endif; ?>

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
