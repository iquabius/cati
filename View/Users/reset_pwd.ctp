<div class="row">
  <div class="col-md-offset-3 col-md-6">
    <h2>Crie uma nova senha para acessar sua conta</h2>
    <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
    <div class="form-group">
      <?php echo $this->Form->label('password', 'Nova Senha', array('class' => 'col-lg-4 control-label'));?>
      <?php echo $this->Form->input('password',array('placeholder' => 'Nova Senha', 'class' => 'form-control', 'label' => false, 'div' => array('class' => 'col-lg-8')));?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->label('password_confirm', 'Confirme Nova Senha', array('class' => 'col-lg-4 control-label'));?>
      <?php echo $this->Form->input('password_confirm',array('type' => 'password', 'placeholder' => 'Confirme Nova Senha', 'class' => 'form-control', 'label' => false, 'div' => array('class' => 'col-lg-8')));?>
    </div>
    <input type="submit" class="btn btn-success"  value="Salvar" />
    <?php echo $this->Form->end(); ?>
  </div>
</div>

<?php
/*if(isset($errors)){
echo '<div class="error">';
echo "<ul>";
foreach($errors as $error){
 echo"<li><div class='error-message'>$error</div></li>";
}
echo"</ul>";
echo'</div>';
}*/
?>
