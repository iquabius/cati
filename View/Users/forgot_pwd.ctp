<h2 class="hightitle">Nos forneça o email com que você se cadastrou</h2>
<?php echo $this->Session->flash(); ?>
<div class="forgot_pwd form row">
  <div class="col-md-8">
    <?php echo $this->Form->create('User', array('action' => 'forgot_pwd', 'class' => 'form-inline row', 'role' => 'form')); ?>
    <div class="form-group col-md-6">
      <?php echo $this->Form->input('email',array('placeholder' => 'Email', 'class' => 'form-control', 'label' => array('class' => 'sr-only', 'text' => 'Email')));?>
    </div>
    <div class="col-md-3">
      <input type="submit" class="btn btn-primary btn-block"  value="Resetar" />
    </div>
    <?php echo $this->Form->end();?>
  </div>

</div>
