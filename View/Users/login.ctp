<div class="users form">
  <?php echo $this->Session->flash('auth'); ?>
  <?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
    <h2 class="form-signin-heading">Login</h2>
    <?php
      echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => 'Email', 'autofocus', 'label' => false));
      echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Senha', 'label' => false));
    ?>
    <!-- <label class="checkbox">
      <input type="checkbox" value="remember-me"> Remember me
    </label> -->
    <?php echo $this->Form->submit('Entrar', array('class' => 'btn btn-lg btn-primary btn-block')); ?>
  <?php echo $this->Form->end(); ?>
</div>
