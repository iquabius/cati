
<div id="page-container" class="row">
  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
          <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">
    <div class="lectures form">
      <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
        <fieldset>
          <h2><?php echo __('Crie sua conta'); ?></h2>
          <div class="form-group">
            <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nome')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('lastname', array('class' => 'form-control', 'label' => 'Sobrenome')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Email')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('email_confirm', array('class' => 'form-control', 'label' => 'Confirme o Email', 'autocomplete' => 'off')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => 'Crie uma Senha')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('cpf', array('class' => 'form-control', 'label' => 'CPF')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => 'Telefone')); ?>
          </div><!-- .form-group -->

          <div class="form-group">
            <?php echo $this->Form->input('institution', array('class' => 'form-control', 'label' => 'Instituição')); ?>
          </div><!-- .form-group -->

        </fieldset>

        <div class="alert alert-warning">
          <b>Formas de Pagamento: </b>
            <ul>
              <li>Depósito ou transferência (Os dados da conta em que receberemos o pagamento pode ser acessado após você fazer o login);</li>
              <li>Pessoalmente, na entrada da UNEMAT na segunda-feira (dia 21), das 19:00 as 22:00</li>
            </ul>
          <b>Valor (Referente às Palestras): </b> R$40.00 (Esse valor inclui uma camiseta do evento);<br>
          Caso pague por depósito ou transferência, será necessário a confirmação do pagamento através do upload de uma cópia do
          comprovante no painel de configuração de sua conta.
        </div>

        <div class="alert alert-warning">
        Inscrição e pagamento dos <?php echo $this->Html->Link('minicursos', array('controller' => 'courses', 'action' => 'index')) ?> serão realizados presencialmente.
        </div><!-- /.alert alert-warning -->

        <?php echo $this->Form->submit('Cadastrar', array('class' => 'btn btn-large btn-primary')); ?>
      <?php echo $this->Form->end(); ?>

    </div><!-- /.form -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
