<!-- view -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
<?php
    // Check if the user is an admin or support
    $loggedUser = $this->Session->read('Auth.User');
    $hasPermission = ($loggedUser && ($loggedUser['role'] == 'admin' || $loggedUser['role'] == 'support'));
?>
        <li class="list-group-item"><?php echo $this->Form->postLink('Excluir Minha Conta', array('action' => 'delete_my_account'), array('class' => 'text-danger'), 'Tem certeza que deseja excluir sua conta? Todos os seus dados serão apagados de nosso banco de dados e não poderão ser recuperados. Seu email e CPF poderão ser usados para criar uma nova conta.'); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link('Comprovar Pagamento', array('action' => 'verify_payment')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link('Editar Meus Dados', array('action' => 'edit_my_data')); ?> </li>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .span3 -->

  <div id="page-content" class="col-sm-9">

    <div class="users view">

      <h2>Minha Conta</h2>

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td><strong><?php echo __('Email'); ?></strong></td>
              <td>
                <?php echo h($user['email']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Nome'); ?></strong></td>
              <td>
                <?php echo h($user['name']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Sobrenome'); ?></strong></td>
              <td>
                <?php echo h($user['lastname']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('CPF'); ?></strong></td>
              <td>
                <?php echo h($user['cpf']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Telefone'); ?></strong></td>
              <td>
                <?php echo h($user['phone']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Instituição'); ?></strong></td>
              <td>
                <?php echo h($user['institution']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Conta Criada em'); ?></strong></td>
              <td>
                <?php echo $this->Time->format('d/m/Y, h:ia', h($user['created']));; ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Última Atualização'); ?></strong></td>
              <td>
                <?php echo $this->Time->format('d/m/Y, h:ia', h($user['modified']));; ?>
              </td>
            </tr>
            <tr class="<?php echo h(__d('cake', $user['payment_status'] . '_css_class')); ?>" title="<?php echo h(__d('cake', $user['payment_status'] . '_description')); ?>">
              <td><strong>Status do Pagamento</strong></td>
              <td>
                <?php echo h(__d('cake', $user['payment_status'])); ?>
              </td>
            </tr>
            <!-- <tr>
              <td><strong><?php echo __('Arquivo de confirmação'); ?></strong></td>
              <td>
                <?php $imgName = h($user['payment_confirm_image']); ?>
                <a href="/img/payment_confirmations/<?php echo $imgName ?>" target="new"><?php echo $imgName ?></a>
              </td>
            </tr> -->
          </tbody>
        </table><!-- /.table table-striped table-bordered -->
      </div><!-- /.table-responsive -->

  <?php if (!empty($user['payment_confirm_image']) && $user['payment_status'] == 'unverified'): ?>
      <div class="alert alert-warning">
        Caso você tenha enviado o comprovante e seu status de pagamento está marcado como "Não Confirmado",
        entre em contato com nossa equipe de suporte (cati@unemat.br) ou reenvie o comprovante.
      </div><!-- /.alert alert-warning -->
  <?php endif; ?>

        <div class="alert alert-warning">
          <b>Dados da conta bancária para o pagamento:</b></br>
          <b>Banco:</b> Banco do Brasil</br>
          <b>Agência:</b> 3196-8</br>
          <b>Conta Corrente:</b> 20264-9</br>
          <b>Títular da Conta:</b> Rodrigo Tasca Pavão</br>
          <b>Valor da inscrição:</b> R$ 40,00 (Esse valor inclui uma camiseta do evento)</br>
        </div><!-- /.alert alert-warning -->

        <div class="alert alert-warning">
        Inscrição e pagamento dos <?php echo $this->Html->Link('minicursos', array('controller' => 'courses', 'action' => 'index')) ?> serão realizados presencialmente.
        </div><!-- /.alert alert-warning -->

    </div><!-- /.view -->

  </div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
