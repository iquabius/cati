<!-- view -->
<div id="page-container" class="row">

  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
<?php
    // Check if the user is an admin or support
    $loggedUser = $this->Session->read('Auth.User');
    $hasPermission = ($loggedUser && ($loggedUser['role'] == 'admin' || $loggedUser['role'] == 'support'));
    if ($hasPermission) :
?>
        <li class="list-group-item"><?php echo $this->Html->link(__('Editar Usuário'), array('action' => 'edit', $user['User']['id']), array('class' => '')); ?> </li>
        <li class="list-group-item"><?php echo $this->Form->postLink(__('Excluir Usuário'), array('action' => 'delete', $user['User']['id']), array('class' => ''), __('Tem certeza que deseja excluir "%s"?', $user['User']['email'])); ?> </li>
<?php endif; ?>
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .span3 -->

  <div id="page-content" class="col-sm-9">

    <div class="users view">

      <h2><?php  echo __('Usuário'); ?></h2>

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td><strong><?php echo __('ID'); ?></strong></td>
              <td>
                <?php echo h($user['User']['id']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Email'); ?></strong></td>
              <td>
                <?php echo h($user['User']['email']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Nome'); ?></strong></td>
              <td>
                <?php echo h($user['User']['name']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Sobrenome'); ?></strong></td>
              <td>
                <?php echo h($user['User']['lastname']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Função'); ?></strong></td>
              <td>
                <?php echo h(__d('cake', $user['User']['role'])); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('CPF'); ?></strong></td>
              <td>
                <?php echo h($user['User']['cpf']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Telefone'); ?></strong></td>
              <td>
                <?php echo h($user['User']['phone']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Instituição'); ?></strong></td>
              <td>
                <?php echo h($user['User']['institution']); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Conta Criada em'); ?></strong></td>
              <td>
                <?php echo $this->Time->format('d/m/Y, h:ia', h($user['User']['created']));; ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Última Atualização'); ?></strong></td>
              <td>
                <?php echo $this->Time->format('d/m/Y, h:ia', h($user['User']['modified']));; ?>
              </td>
            </tr>
            <tr class="<?php echo h(__d('cake', $user['User']['payment_status'] . '_css_class')); ?>" title="<?php echo h(__d('cake', $user['User']['payment_status'] . '_description')); ?>">
              <td><strong>Status do Pagamento</strong></td>
              <td>
                <?php echo h(__d('cake', $user['User']['payment_status'])); ?>
              </td>
            </tr>
            <tr>
              <td><strong><?php echo __('Arquivo de confirmação'); ?></strong></td>
              <td>
                <?php $imgName = h($user['User']['payment_confirm_image']); ?>
                <a href="<?php echo $this->webroot ?>img/payment_confirmations/<?php echo $imgName ?>" target="new"><?php echo $imgName ?></a>
              </td>
            </tr>
          </tbody>
        </table><!-- /.table table-striped table-bordered -->
      </div><!-- /.table-responsive -->

    </div><!-- /.view -->

      <div class="related">

        <h3>Dias em que esteve presente</h3>

        <?php if (!empty($user['Day'])): ?>

          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th><?php echo __('Título'); ?></th>
                  <th class="actions2"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $i = 0;
                  foreach ($user['Day'] as $day): ?>
                  <tr>
                      <td><?php echo $this->Html->link($day['title'], array('controller' => 'days', 'action' => 'view', $day['id']), array('class' => '')); ?></td>
                      <td class="actions2">
                        <?php echo $this->Form->postLink(__('Remover'), array('controller' => 'days', 'action' => 'remove_attendance_verification', $day['id'], $user['User']['id']), array('class' => 'btn btn-default btn-xs btn-danger'), __('Tem certeza que deseja desfazer essa verificação de presença?')); ?>
                      </td>
                    </tr>
                <?php endforeach; ?>
              </tbody>
            </table><!-- /.table table-striped table-bordered -->
          </div><!-- /.table-responsive -->

        <?php else: ?>
          <p class="text-muted"><i>Este usuário não está marcado como presente em nenhum dos dias.</i></p>
        <?php endif; ?>

      </div><!-- /.related -->

  </div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
