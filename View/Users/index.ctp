
<div id="page-container" class="row">
  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
        <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">
    <div class="users index">
      <h2><?php echo 'Usuários'; ?></h2>
      <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
          <thead>
            <tr>
              <!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
              <th><?php echo $this->Paginator->sort('email'); ?></th>
              <th><?php echo $this->Paginator->sort('name', 'Nome'); ?></th>
              <th><?php echo $this->Paginator->sort('lastname', 'Sobrenome'); ?></th>
              <th><?php echo $this->Paginator->sort('cpf', 'CPF'); ?></th>
              <th><?php echo $this->Paginator->sort('payment_status', 'Status de Pagamento'); ?></th>
              <th class="actions"></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($users as $user): ?>
              <tr  class="<?php echo h(__d('cake', $user['User']['payment_status'] . '_css_class')); ?>" title="<?php echo h(__d('cake', $user['User']['payment_status'] . '_description')); ?>">
                <!-- <td><?php echo h($user['User']['id']); ?>&nbsp;</td> -->
                <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['lastname']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['cpf']); ?>&nbsp;</td>
                <td>
                  <?php echo h(__d('cake', $user['User']['payment_status'])); ?>&nbsp;
                </td>
                <td class="actions">
                  <?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-default btn-xs')); ?>
                  <?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-default btn-xs')); ?>
                  <?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-default btn-xs btn-danger'), __('Tem certeza que deseja excluir "%s"?', $user['User']['email'])); ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div><!-- /.table-responsive -->

      <p><small>
        <?php
        echo $this->Paginator->counter(array(
        'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de um total de {:count}, começando do registro {:start}, terminando em {:end}')
        ));
        ?>
      </small></p>

      <ul class="pagination">
        <?php
          echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
          echo $this->Paginator->next(__('Próximo') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
      </ul><!-- /.pagination -->

    </div><!-- /.index -->

  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
