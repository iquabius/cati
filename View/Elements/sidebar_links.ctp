
        <li class="list-group-item"><?php echo $this->Html->link(__('Lista de Palestras'), array('controller' => 'lectures', 'action' => 'index')); ?></li>
        <li class="list-group-item"><?php echo $this->Html->link(__('Lista de Minicursos'), array('controller' => 'courses', 'action' => 'index')); ?></li>
<?php
    // Check if the user is an admin or support
    $user = $this->Session->read('Auth.User');
    $hasPermission = ($user && ($user['role'] == 'admin' || $user['role'] == 'support'));
    if ($hasPermission) :
?>
        <li class="list-group-item"><?php echo $this->Html->link(__('Lista de Usuários'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link(__('Adicionar Palestra'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link(__('Adicionar Minicurso'), array('controller' => 'courses', 'action' => 'add')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link(__('Lista de Dias'), array('controller' => 'days', 'action' => 'index')); ?> </li>
        <li class="list-group-item"><?php echo $this->Html->link(__('Adicionar Dia'), array('controller' => 'days', 'action' => 'add')); ?> </li>
<?php endif; ?>
