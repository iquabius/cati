<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".bs-navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button><!-- /.navbar-toggle -->
    <?php echo $this->Html->Link('<img src="' . $this->webroot . 'img/cati_logo.png" />', array('controller' => 'cati', 'action' => 'home'), array('escape' => false, 'class' => 'logo')); ?>
  </div><!-- /.navbar-header -->
  <div class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
    <ul class="nav navbar-nav">
      <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='home') )?'active':'' ?>">
        <?php echo $this->Html->Link('Início', array('controller' => 'cati', 'action' => 'home')); ?>
      </li>
      <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='cati2013') )?'active':'' ?>">
        <?php echo $this->Html->Link('CATI 2013', array('controller' => 'cati', 'action' => 'cati2013')); ?>
      </li>
      <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='contact') )?'active':'' ?>">
        <?php echo $this->Html->Link('Contato', array('controller' => 'cati', 'action' => 'contact')); ?>
      </li>
    </ul><!-- /.nav navbar-nav -->

    <ul class="nav navbar-nav navbar-right">
    <?php if ($user = $this->Session->read('Auth.User')): ?>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bem vindo, <b><?php echo $user['name'] ?></b> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><?php echo $this->Html->Link('<span class="glyphicon glyphicon-cog"></span> Minha Conta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('controller' => 'users', 'action' => 'my_account'), array('escape' => false)); ?></li>
          <li class="divider"></li>
          <li><?php echo $this->Html->Link('<span class="glyphicon glyphicon-off"> Sair', array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?></li>
        </ul>
      </li>
    <?php else: ?>
      <li><?php echo $this->Html->link('Inscreva-se', array('controller' => 'users', 'action' => 'add')); ?></li>
      <li class="dropdown">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">Entrar <strong class="caret"></strong></a>
        <div class="dropdown-menu" style="padding: 10px; min-width: 250px;">
          <?php
            echo $this->Form->create('User', array('controller' => 'users', 'action' => 'login', 'class' => 'form-signin-widget', 'accept-charset' => 'UTF-8'));
              echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => 'Email', 'label' => false));
              echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Senha', 'label' => false));
          ?>
            <?php echo $this->Form->submit('Entrar', array('class' => 'btn btn-primary btn-block')); ?>
            <?php echo $this->Html->Link('Resetar senha', array('controller' => 'users', 'action' => 'forgot_pwd'), array('class' => 'btn btn-primary btn-block')); ?>
          <?php echo $this->Form->end(); ?>
        </div>
      </li>
    <?php endif; ?>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav><!-- /.navbar navbar-default -->
