<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<?php echo $this->Html->docType('html5'); ?>
<html>
  <head>
    <?php echo $this->Html->charset(); ?>
    <link rel="author" href="humans.txt" />
    <title>
      CATI -
      <?php echo $title_for_layout; ?>
    </title>
    <?php
      echo $this->Html->meta('icon');

      echo $this->fetch('meta');

      echo $this->Html->css('bootstrap.min');
      // Uncomment this to enable the bootstrap gradient theme (Flat is way better though).
      //echo $this->Html->css('bootstrap-theme.min');
      echo $this->Html->css('core');

      echo $this->fetch('css');

      echo $this->Html->script('libs/jquery-1.10.2.min');
      echo $this->Html->script('libs/bootstrap.min');

      echo $this->fetch('script');
    ?>
    <!-- http://fortawesome.github.io/Font-Awesome/icons/ -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <style type="text/css">
    html,
    body {
      height: 100%;
      /* The html and body elements cannot have any padding or margin. */
    }

    /* Wrapper for page content to push down footer */
    #content_wrapper {
      min-height: 100%;
      height: auto !important;
      height: 100%;
      /* Negative indent footer by its height */
      margin: 0 auto -80px;
      /* Pad bottom by footer height */
      padding: 0 0 80px;
    }

    #main_wrapper {
      min-height: 100%;
      height: 100%;
      position: absolute;
      z-index: 2;
      width: 100%;
    }

    /* Set the fixed height of the footer here */
    #footer {
      padding-bottom: 20px;
      font-size: 12px;
    }
    #footer .container div {
      min-height: 60px;
      background-color: #f8f8f8;
      border: 1px solid #e7e7e7;
      border-radius: 4px;
      padding: 14px;
    }
    #footer span.credits {
      float: right;
      padding-top: 6px;
    }
    #footer .license .license-logo:hover {
      text-decoration: none;
    }
    </style>
  </head>

  <body>
    <div id="main_wrapper">
      <div id="content_wrapper">

        <div id="main-container" class="container">

          <div id="header" class="">
            <?php echo $this->element('menu/top_menu'); ?>
          </div><!-- /#header .container -->

          <div id="content" class="">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
          </div><!-- /#header .container -->

        </div><!-- /#main-container -->

      </div>

      <div id="footer">
        <div class="container"><div>
          <span class="text-muted credits">
            Desenvolvido por <a href="<?php echo $this->webroot; ?>humans.txt" target="_blank">Josias Duarte Busiquia</a>
          </span>
          <span class="license">
            <a class="license-logo" rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/br/" target="_blank">
              <img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" />
            </a>
            <span class="text-muted">Este site está licenciado sob uma <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/br/" target="_blank">licença Creative Commons Atribuição-CompartilhaIgual 3.0 Brasil</a>.</span>
          </span>
        </div></div>
      </div>

    </div><!-- #main_wrapper -->
      <?php
        echo $this->fetch('bottomScripts');
        echo $this->fetch('inlineScript');
      ?>
  </body>

</html>
