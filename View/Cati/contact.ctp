
<!-- Contacts -->
<div id="contacts">
  <div class="row">
    <!-- Alignment -->
    <div class="col-sm-9">
       <!-- Form itself -->
      <form name="sentMessage" class="inline-form" id="contactForm" role="form" novalidate>
        <h1>Entre em contato</h1>
        <div class="row">
          <div class="col-md-12">
            <p><b>Email</b>: cati@unemat.br</p>
            <address>
              <strong>UNEMAT, Campus de Barra do Bugres</strong><br>
              Rua A, S/N, Cohab São Raimundo, cx. postal 92<br>
              CEP: 78.390-000<br>
              Fone: (65) 9628-6858 (Rodrigo Pavão)<br>
              Barra do Bugres / MT
            </address>
          </div>
        </div>
      </form>
    </div>
    <?php echo $this->element('sidebar'); ?>
  </div>
</div>
