<div class="row">
  <div class="col-md-9">
    <div id="carousel-example-generic" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="<?php echo $this->webroot; ?>img/slide.jpg" class="img-responsive">
            </div>
            <div class="item">
                <img src="<?php echo $this->webroot; ?>img/slide_2.png" class="img-responsive">
            </div>
        </div>

        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
    <?php echo $this->element('sponsors'); ?>
  </div>
  <?php echo $this->element('sidebar'); ?>
</div>
