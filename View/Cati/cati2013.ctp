
<div id="page-container" class="row">
  <div id="sidebar" class="col-sm-3">
    <div class="actions">
      <ul class="list-group">
          <?php echo $this->element('sidebar_links'); ?>
      </ul><!-- /.list-group -->
    </div><!-- /.actions -->
  </div><!-- /#sidebar .col-sm-3 -->

  <div id="page-content" class="col-sm-9">
    <div class="about-cati2013">
        <h2>6º Congresso Acadêmico de Tecnologia e Informática – CATI 2013</h2>
        <p>
          De 22 a 25 de outubro de 2013, acontece nas dependências da Universidade do Estado de Mato Grosso,
          em Barra do Bugres/MT, o 6º Congresso Acadêmico de Tecnologia e Informática – CATI, o evento
          proporcionará o encontro de estudantes, professores, pesquisadores, empresários e técnicos da
          área de tecnologia da informação em diversos setores para explanarem sobre as principais tendências
          da era digital relacionadas à inovação, ciência, tecnologia e entretenimento.
        </p>
        <p>
          O evento promoverá <?php echo $this->Html->Link('minicursos', array('controller' => 'courses', 'action' => 'index')) ?>,
          <?php echo $this->Html->Link('palestras', array('controller' => 'lectures', 'action' => 'index')) ?>,
          apresentações, oficinas, reuniões, atrações culturais e discussões voltadas à tecnologia da
          informação, sendo a temática da 6ª edição do evento estabelecida em “Desenvolvimento de Jogos e Entretenimento Digital”.
        </p>
    </div>
  </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
